package ru.adonixis.animatedmenuitems.entry.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ru.adonixis.animatedmenuitems.*;
import ru.adonixis.animatedmenuitems.entry.ResourceTable;

public class MainAbilitySlice extends AbilitySlice {
    private CopyComponent copyComponent;
    private CutComponent cutComponent;
    private DeleteComponent deleteComponent;
    private ShareComponent shareComponent;
    private AddComponent addComponent;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        copyComponent = (CopyComponent) findComponentById(ResourceTable.Id_copy);
        copyComponent.setClickedListener(component -> copyComponent.startAnimator());
        cutComponent = (CutComponent) findComponentById(ResourceTable.Id_cut);
        cutComponent.setClickedListener(component -> cutComponent.startAnimator());
        deleteComponent = (DeleteComponent) findComponentById(ResourceTable.Id_delete);
        deleteComponent.setClickedListener(component -> deleteComponent.startAnimator());
        shareComponent = (ShareComponent) findComponentById(ResourceTable.Id_share);
        shareComponent.setClickedListener(component -> shareComponent.startAnimator());
        addComponent = (AddComponent) findComponentById(ResourceTable.Id_add);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
