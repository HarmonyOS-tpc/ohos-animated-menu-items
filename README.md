作者声明：
这里保留原有的xml矢量图的加载，通过openharmony的方式使用自定义Component加AnimatorValue方式替代原有的xml动画效果，效果而言是与原库完全一致的。
同时增加原有自带的点击效果，并且可以通过xml传参或api调取方式设置开关与颜色。
就使用上app开发者可以自由把控。

# 在线依赖:
```groovy
dependencies{
    implementation 'io.openharmony.tpc.thirdlib:ohos-animated-menu-items:1.0.1'
}
```

# 演示图:

<img src="art/demo.gif" width="40%"/>

# AttrSet:

|name|format|description|
|:---:|:---:|:---:|
| rippleEnabled | boolean | 设置点击效果
| rippleAlpha | float | 设置点击效果透明度
| rippleColor | color | 设置点击效果颜色

License
======

```
   Copyright 2017 Ilya Fomenko

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```
