/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.adonixis.animatedmenuitems;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.VectorElement;
import ohos.app.Context;

/**
 * the share component
 */
public class ShareComponent extends AnimatorComponent {
    private VectorElement roundElement;
    private VectorElement topLineOneElement;
    private VectorElement topLineTwoElement;
    private VectorElement bottomLineOneElement;
    private VectorElement bottomLineTwoElement;
    private Component round;
    private Component topLineOne;
    private Component topLineTwo;
    private Component bottomLineOne;
    private Component bottomLineTwo;

    public ShareComponent(Context context) {
        this(context, null);
    }

    public ShareComponent(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public ShareComponent(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context);
    }

    private void init(Context context) {
        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        round = new Component(context);
        topLineOne = new Component(context);
        topLineTwo = new Component(context);
        bottomLineOne = new Component(context);
        bottomLineTwo = new Component(context);
        addComponent(round, layoutConfig);
        addComponent(topLineOne, layoutConfig);
        addComponent(topLineTwo, layoutConfig);
        addComponent(bottomLineOne, layoutConfig);
        addComponent(bottomLineTwo, layoutConfig);
    }

    private void reset() {
        float topLineOneScale = 0;
        float topLineTwoScale = 1;
        float bottomLineOneScale = 0;
        float bottomLineTwoScale = 1;
        topLineOne.setScale(topLineOneScale, topLineOneScale);
        topLineTwo.setScale(topLineTwoScale, topLineTwoScale);
        bottomLineOne.setScale(bottomLineOneScale, bottomLineOneScale);
        bottomLineTwo.setScale(bottomLineTwoScale, bottomLineTwoScale);
    }

    /**
     * start animator
     */
    public void startAnimator() {
        animatorDurationTime = 700;
        clickAnimator.setDuration(350);
        clickAnimator.setLoopedCount(1);
        super.startAnimator();
    }

    @Override
    public void onEnd(Animator animator) {
        reset();
        super.onEnd(animator);
    }

    @Override
    public void onUpdate(AnimatorValue animatorValue, float v) {
        float duration = v * 350;
        float topLineOneScale = 0;
        float topLineTwoScale = 1;
        float bottomLineOneScale = 0;
        float bottomLineTwoScale = 1;
        if (duration >= 50 && duration <= 200) {
            topLineOneScale = (duration - 50) / 150;
        } else if (duration > 200) {
            topLineOneScale = 1;
        }
        if (duration <= 150) {
            topLineTwoScale = (150 - duration) / 150;
        } else {
            topLineTwoScale = 0;
        }

        if (duration >= 200) {
            bottomLineOneScale = (duration - 200) / 150;
        }

        if (duration >= 150 && duration <= 300) {
            bottomLineTwoScale = (300 - duration) / 150;
        } else if (duration > 300) {
            bottomLineTwoScale = 0;
        }

        topLineOne.setPivot(18f * topLineOne.getWidth() / 24, 5f * topLineOne.getHeight() / 24);
        topLineTwo.setPivot(6f * topLineTwo.getWidth() / 24, 12f * topLineTwo.getHeight() / 24);
        bottomLineOne.setPivot(6f * bottomLineOne.getWidth() / 24, 12f * bottomLineOne.getHeight() / 24);
        bottomLineTwo.setPivot(18f * bottomLineTwo.getWidth() / 24, 19f * bottomLineOne.getHeight() / 24);
        topLineOne.setScale(topLineOneScale, topLineOneScale);
        topLineTwo.setScale(topLineTwoScale, topLineTwoScale);
        bottomLineOne.setScale(bottomLineOneScale, bottomLineOneScale);
        bottomLineTwo.setScale(bottomLineTwoScale, bottomLineTwoScale);
    }

    @Override
    public void onRefreshed(Component component) {
        roundElement = new VectorElement(getContext(), ResourceTable.Graphic_ic_share_round_white_24dp);
        roundElement.setAntiAlias(true);
        round.setBackground(roundElement);
        topLineOneElement = new VectorElement(getContext(), ResourceTable.Graphic_ic_share_top_line_1_white_24dp);
        topLineOneElement.setAntiAlias(true);
        topLineOne.setBackground(topLineOneElement);
        topLineTwoElement = new VectorElement(getContext(), ResourceTable.Graphic_ic_share_top_line_2_white_24dp);
        topLineTwoElement.setAntiAlias(true);
        topLineTwo.setBackground(topLineTwoElement);
        bottomLineOneElement = new VectorElement(getContext(), ResourceTable.Graphic_ic_share_bottom_line_1_white_24dp);
        bottomLineOneElement.setAntiAlias(true);
        bottomLineOne.setBackground(bottomLineOneElement);
        bottomLineTwoElement = new VectorElement(getContext(), ResourceTable.Graphic_ic_share_bottom_line_2_white_24dp);
        bottomLineTwoElement.setAntiAlias(true);
        bottomLineTwo.setBackground(bottomLineTwoElement);
        reset();
        invalidate();
    }
}
