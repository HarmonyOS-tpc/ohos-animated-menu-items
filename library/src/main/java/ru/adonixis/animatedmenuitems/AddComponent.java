/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.adonixis.animatedmenuitems;

import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.VectorElement;
import ohos.app.Context;

/**
 * the add component
 */
public class AddComponent extends AnimatorComponent {
    private VectorElement addElement;
    private Component add;

    public AddComponent(Context context) {
        this(context, null);
    }

    public AddComponent(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public AddComponent(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context);
    }

    private void init(Context context) {
        add = new Component(context);
        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        addComponent(add, layoutConfig);
    }

    @Override
    public void onUpdate(AnimatorValue animatorValue, float v) {
    }

    @Override
    public void onRefreshed(Component component) {
        addElement = new VectorElement(getContext(), ResourceTable.Graphic_ic_add_white_24dp);
        addElement.setAntiAlias(true);
        add.setBackground(addElement);
        invalidate();
    }
}
