/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.adonixis.animatedmenuitems;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.VectorElement;
import ohos.app.Context;

/**
 * the copy component
 */
public class CopyComponent extends AnimatorComponent {
    private VectorElement sheetElement;
    private VectorElement lineElement;
    private Component sheet;
    private Component line;

    public CopyComponent(Context context) {
        this(context, null);
    }

    public CopyComponent(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public CopyComponent(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context);
    }

    private void init(Context context) {
        line = new Component(context);
        sheet = new Component(context);
        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        addComponent(line, layoutConfig);
        addComponent(sheet, layoutConfig);
    }

    private void reset() {
        sheet.setTranslation(0, 0);
    }

    /**
     * start animator
     */
    public void startAnimator() {
        animatorDurationTime = 600;
        clickAnimator.setDuration(600);
        super.startAnimator();
    }

    @Override
    public void onUpdate(AnimatorValue animatorValue, float v) {
        int width = sheet.getWidth();
        int height = sheet.getHeight();
        float fraction;
        if (v < 0.5f) {
            fraction = v * 2;
        } else {
            fraction = (1 - v) * 2;
        }
        sheet.setTranslation(-fraction * width / 6, -fraction * height / 6);
    }

    @Override
    public void onRefreshed(Component component) {
        sheetElement = new VectorElement(getContext(), ResourceTable.Graphic_ic_copy_sheet_white_24dp);
        lineElement = new VectorElement(getContext(), ResourceTable.Graphic_ic_copy_line_white_24dp);
        sheetElement.setAntiAlias(true);
        lineElement.setAntiAlias(true);
        sheet.setBackground(sheetElement);
        line.setBackground(lineElement);
        reset();
        invalidate();
    }

    @Override
    public void onEnd(Animator animator) {
        reset();
        super.onEnd(animator);
    }
}
