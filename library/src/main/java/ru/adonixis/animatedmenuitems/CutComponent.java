/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.adonixis.animatedmenuitems;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.VectorElement;
import ohos.app.Context;

/**
 * the cup component
 */
public class CutComponent extends AnimatorComponent {
    private VectorElement firstBladeElement;
    private VectorElement secondBladeElement;
    private Component firstBlade;
    private Component secondBlade;

    public CutComponent(Context context) {
        this(context, null);
    }

    public CutComponent(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public CutComponent(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context);
    }

    private void init(Context context) {
        firstBlade = new Component(context);
        secondBlade = new Component(context);
        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        addComponent(firstBlade, layoutConfig);
        addComponent(secondBlade, layoutConfig);
    }

    private void reset() {
        firstBlade.setRotation(0);
        secondBlade.setRotation(0);
    }

    /**
     * start animator
     */
    public void startAnimator() {
        animatorDurationTime = 600;
        clickAnimator.setDuration(300);
        clickAnimator.setLoopedCount(1);
        super.startAnimator();
    }

    @Override
    public void onEnd(Animator animator) {
        reset();
        super.onEnd(animator);
    }

    @Override
    public void onUpdate(AnimatorValue animatorValue, float v) {
        float fraction;
        if (v < 0.5f) {
            fraction = v * 2;
        } else {
            fraction = (1 - v) * 2;
        }
        firstBlade.setRotation(20 * fraction);
        secondBlade.setRotation(-20 * fraction);
    }

    @Override
    public void onRefreshed(Component component) {
        firstBladeElement = new VectorElement(getContext(), ResourceTable.Graphic_ic_cut_first_blade_white_24dp);
        secondBladeElement = new VectorElement(getContext(), ResourceTable.Graphic_ic_cut_second_blade_white_24dp);
        firstBladeElement.setAntiAlias(true);
        secondBladeElement.setAntiAlias(true);
        firstBlade.setBackground(firstBladeElement);
        secondBlade.setBackground(secondBladeElement);
        reset();
        invalidate();
    }
}
