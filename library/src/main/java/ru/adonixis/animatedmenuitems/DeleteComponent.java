/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.adonixis.animatedmenuitems;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.VectorElement;
import ohos.app.Context;

/**
 * the delete component
 */
public class DeleteComponent extends AnimatorComponent {
    private VectorElement capElement;
    private VectorElement boxElement;
    private Component cap;
    private Component box;

    public DeleteComponent(Context context) {
        this(context, null);
    }

    public DeleteComponent(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public DeleteComponent(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context);
    }

    private void init(Context context) {
        box = new Component(context);
        cap = new Component(context);
        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        addComponent(box, layoutConfig);
        addComponent(cap, layoutConfig);
    }

    private void reset() {
        cap.setTranslation(0, 0);
        cap.setRotation(0);
    }

    /**
     * start animator
     */
    public void startAnimator() {
        animatorDurationTime = 620;
        clickAnimator.setDuration(620);
        super.startAnimator();
    }

    @Override
    public void onUpdate(AnimatorValue animatorValue, float v) {
        int width = cap.getWidth();
        int height = cap.getHeight();
        float duration = v * animatorValue.getDuration();
        float translateY;
        float translateYMax = height * -1.5f / 24;
        if (duration < 100) {
            translateY = translateYMax * duration / 100;
        } else if (duration > 520) {
            translateY = translateYMax * (620 - duration) / 100;
        } else {
            translateY = translateYMax;
        }
        float rotation;
        float maxRotation = 15;
        float minRotation = -15;
        if (duration > 100 && duration < 110) {
            rotation = maxRotation * (duration - 100) / 10;
        } else if (duration > 510 && duration < 520) {
            rotation = maxRotation * (520 - duration) / 10;
        } else if (duration >= 110 && duration <= 210) {
            rotation = maxRotation + (minRotation - maxRotation) * (duration - 110) / 100;
        } else if (duration >= 210 && duration <= 310) {
            rotation = minRotation + (maxRotation - minRotation) * (duration - 210) / 100;
        } else if (duration >= 310 && duration <= 410) {
            rotation = maxRotation + (minRotation - maxRotation) * (duration - 310) / 100;
        } else if (duration >= 410 && duration <= 510) {
            rotation = minRotation + (maxRotation - minRotation) * (duration - 410) / 100;
        } else {
            rotation = 0;
        }
        cap.setPivot(width * 12f / 24, height * 4.5f / 24);
        cap.setRotation(rotation);
        cap.setTranslationY(translateY);
    }

    @Override
    public void onRefreshed(Component component) {
        capElement = new VectorElement(getContext(), ResourceTable.Graphic_ic_delete_cap_white_24dp);
        boxElement = new VectorElement(getContext(), ResourceTable.Graphic_ic_delete_box_white_24dp);
        capElement.setAntiAlias(true);
        boxElement.setAntiAlias(true);
        cap.setBackground(capElement);
        box.setBackground(boxElement);
        reset();
        invalidate();
    }

    @Override
    public void onEnd(Animator animator) {
        reset();
        super.onEnd(animator);
    }
}
